# UnnamedProject

A project that doesn't have a name yet


## Disclaimer: This is pre-pre-pre-alpha software. Many features are not implemented yet.

### How to install:

- Download a Void Musl live ISO and boot to it on the system you wanna install to
- After booting into the live ISO, run the following commands to fetch the installer:

```
xbps-install -Suy xbps # XBPS won't install anything until it's been updated first
xbps-install -y curl # Install cURL, needed for fetching the installer script
curl https://git.envs.net/NobodySpecial/UnnamedProject/raw/branch/master/psec-install.sh > psec-install.sh
```
Once you've fetched the installer, run it by running
```
sudo bash psec-install.sh
```

#### NOTE: The installer requires internet access.

If an install fails and you want to try again, you'll need to unmount the volumes created during the install process. The installer doesn't do this on its own. However, you can get it to do this with the following command:
```
reinstall=y bash psec-install.sh
```
This will tell it to attempt reinstallation after a previous failed installation. It may also unmount any unrelated volumes you have connected, so you'll have to remount them.


### Why do I need to manually download the installer?

While we're planning to release a live ISO for this OS, this hasn't been done yet. In the meantime, this is the install process until the first public alpha release.

Other expected features:

- A fully-offline installer
- Separate installer packages with preconfigured DEs/WMs
- Full verified boot without compromising user freedom or requiring vendor lock-in
	- Current verified boot setups rely on secret keys stored on other systems. This OS will feature a separate "update mode" for performing system management and updates
- A large amount of system hardening by default

### Features that aren't implemented yet:

- A usable base
	- Work on this is in progress
- Verified boot
	- This will be implemented after the base is working
- System hardening
	- This will be implemented after verified boot is working
- DE/WM integration
	- After the hardened base is setup, work will begin on developing install images with DEs/WMs preinstalled. Sway will be the first to get support. After that, it's a matter of what people want added
	- The release of a Sway-supported build is intended to mark the first production-ready build of this OS
- Offline installer
	- This will be implemented when/if I get around to it. No promises
- Other usability features
	- Security comes first, second, and third. Once a suitable secure base is setup, usability features can be added

### Can I run this in a VM?

No. This installer fails to work on both QEMU and VirtualBox. It has not been tested with other hypervisors, but I expect the answer will remain the same.
