#!/bin/bash

OS_NAME="UnnamedProject (based on Void Musl)"
cat << EOF
Welcome to the $OS_NAME installer!

This installer is still in pre-alpha, so be sure to report any bugs you find to:
@neobabba:envs.net

This project can be found at:
	https://git.envs.net/NobodySpecial/UnnamedProject

Press ENTER to continue...
EOF
read -s

umask=0077 # Set a more secure umask when setting up the new system

# For testing purposes:
# Unmount volumes created in a previous run of this installer
if [[ "$reinstall" == "y" ]]; then
	echo "Unmounting existing mount points..."
        umount /mnt/boot/efi # Unmount the recently-installed ESP
	umount /mnt/boot # Unmount the boot partition
        for g in 1 2; do # hard-coding 2 loops is easier than changing the unmounts when the partition layout is updated
                for f in /dev/mapper/*; do # This uses a very dumb method: unmount every /dev/mapper device. But it works
                        umount "$f" # When it gets to `/mnt` it'll fail to unmount. The second iteration will catch it
                        cryptsetup luksClose "$f"
                done
        done
	echo "Done."
fi
#

# First things first, install dependencies
SSL_NO_VERIFY_PEER=0 xbps-install -Suy xbps
#xbps-install -Suy # Update all packages in the installer
SSL_NO_VERIFY_PEER=0 xbps-install -y wget xz python

lsblk
dev=""
while [[ "$dev" == "" ]]; do
	echo -n "What disk to install to? "
	read dev
done
if [[ "$dev" == "" ]]; then
	echo "Aborting."
	exit
fi
disk="/dev/$dev"

sector_size="$(cat /sys/block/"$dev"/queue/hw_sector_size)"
echo "Sector size: $sector_size"
# Create a new GPT partition table on $disk
echo "label: gpt" | sfdisk "$disk"
meg="$((1048576/"$sector_size"))"

# Partition $disk with

# 1 EFI partition (512M) used for UEFI boot
# 1 dm-verity (verified boot) partition (8G) used for the filesystem root
# 1 partition used for storing the verified boot signing key
# 1 boot flags partition (128M) used for rebooting into update mode
# 1 partition (128M) used for storing the FDE keyfile
# 1 partition (9G) used for temporary storage of data while in update mode
# 1 verity hash partition (512M each) used to verify the integrity of the verified boot partitions mentioned earlier
# and a /home partition spanning the rest of the disk

# Note that before attempting to modify the contents of the dm-verity partition, users must reboot into update mode
# Rebooting into update mode will remount the verified boot partitions as read-write, then recalculate and sign the new
# hash tree after the user commits the changes

sfdisk "$disk" << EOF
,$((512*$meg)),L
,$((8192*$meg)),L
,$((1024*$meg)),L
,$((128*$meg)),L
,$((9216*$meg)),L
,$((8192*$meg)),L
,$((512*$meg)),L
,,L;
EOF
#sfdisk --part-attrs "$disk" 7 4
lsblk
home=8

#clear

keyfile="/tmp/$(cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c24)"
boot_keyfile="/tmp/$(cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c24)"
cat /dev/urandom | head -c8192 > "$keyfile" # 8192 bytes is massive overkill and completely pointless, since 32 bytes already provides 256 bits of security... But larger numbers look more secure, and there's not enough of a performance hit for the user to notice anyway
cat /dev/urandom | head -c8192 > "$boot_keyfile"

cryptsetup luksFormat -q --key-file="$keyfile" "$disk"2  --type luks1 # dm-verity partition A (primary)

cat << EOF
To protect your system and implement verified boot, installing updates and software packages requires you to reboot into 'update mode'
In order to ensure your system's verified boot security, you must set a secure password, which will be required to enter update mode.
If an attacker gets this password, the only thing keeping them from installing peristent malware on your system is the user-root boundary.
For high threat models, it may be assumed that an attacker could get root. As such, this password will prevent compromises from persisting across reboots.

EOF
cat << EOF
You will now set a bootloader password. This will protect your bootloader from malware on your main OS tampering with it.
This should be protected the same as your update mode password, since an attacker that gets this password can inject malware into your boot process.
An attacker who manages that would be able to bypass the security of verified boot. Thus, this password must be secure.
Your main system should not be trusted to know this password, since the purpose of this being separate is to protect your bootloader against your
main system. As such, you must never enter your password except at first boot.

EOF
cat << EOF
Previous versions of this installer used separate passwords for update mode and booting. For user convenience, these passwords are now merged into a single password.

You will now be prompted for the update+boot password
EOF
echo -n "Password: "
read -s updpwd
echo
echo -n "Again: "
read -s updpwd2
echo

# Use a random salt as a blinding factor. This can improve security in the case of timing side-channels against string comparison
salt="$(cat /dev/urandom | tr -dc [:print:] | head -c48)"

sum1="$(echo "$salt$updpwd" | b2sum)"
sum2="$(echo "$salt$updpwd2" | b2sum)"

if [[ "$sum1" != "$sum2" ]]; then
	echo "Passwords don't match."
	exit
fi

# Void's kernel zeroes freed memory by default. As such, all that's needed to securely erase the password is to deallocate/reallocate its memory
updpwd2=""
salt="$(cat /dev/urandom | tr -dc [:print:] | head -c48)"
sum1=""
sum2=""

cat << EOF
You will now set a password for your system's full-disk encryption. This password will be required to boot into your system.
This password must be secure, since an attacker who gets this password can read all the files on your hard drive.

EOF

cryptsetup luksAddKey "$disk"2 --key-file="$keyfile" || exit

#cat << EOF
#You will now set a bootloader password. This will protect your bootloader from malware on your main OS tampering with it.
#This should be protected the same as your update mode password, since an attacker that gets this password can inject malware into your boot process.
#An attacker who manages that would be able to bypass the security of verified boot. Thus, this password must be secure.
#Your main system should not be trusted to know this password, since the purpose of this being separate is to protect your bootloader against your
#main system. As such, you must never enter your password except at first boot.
#
#EOF

echo -n "$updpwd" | cryptsetup luksFormat -q "$disk"3 --key-file - # update mode signing key store
echo -n "$updpwd" | cryptsetup luksOpen "$disk"3 part3 --key-file -

# Modern cryptsetup builds default to AES-XTS-plain64 with a 512-bit key
# Due to weirdness with cryptsetup's argument semantics and XTS's design, this corresponds to a 256-bit AES key
# XTS is desirable since it guarantees any modification to the input completely randomizes the plaintext
# This forms a pseudo-authenticated encryption mode, since if the system tries using corrupted plaintext to boot,
# it'll load invalid data and cause the boot process to fail
# This protects the bootloader, its configuration, and the vmlinuz image from tampering
echo -n "$updpwd" | cryptsetup luksFormat -q "$disk"7 --type luks1 --key-file -
echo -n "$updpwd" | cryptsetup luksOpen "$disk"7 part7 --key-file -

updpwd="" # We're done with this, so it's time to erase it from memory

mkfs -t vfat "$disk"1 # EFI partition
cryptsetup luksFormat -q --key-file="$keyfile" "$disk"4 # Boot flags. These are used to determine when to enter "update mode" # use --integrity to ensure only attackers who've unlocked the disk can modify boot flags
cryptsetup luksFormat -q --key-file="$keyfile" "$disk""$home" # /home, BTRFS because it can detect changes made by attackers without the FDE key, as well as accidental corruptions. It's also more resilient than ext4

cryptsetup luksFormat -q --key-file="$boot_keyfile" "$disk"5 # This partition will be wiped and reformatted every time the user enters update mode
cryptsetup luksFormat -q --key-file="$keyfile" "$disk"6

cryptsetup luksOpen --key-file="$keyfile" "$disk"2 part2
cryptsetup luksOpen --key-file="$keyfile" "$disk"4 part4
cryptsetup luksOpen --key-file="$keyfile" "$disk""$home" homepart

cryptsetup luksOpen --key-file="$boot_keyfile" "$disk"5 part5
cryptsetup luksOpen --key-file="$keyfile" "$disk"6 part6

#clear

mkfs.btrfs /dev/mapper/part2 --checksum blake2 -f
mkfs.btrfs /dev/mapper/part3 --checksum blake2 -f
mkfs.btrfs /dev/mapper/part4 --checksum blake2 -f
mkfs.btrfs /dev/mapper/part5 --checksum blake2 -f
mkfs.btrfs /dev/mapper/part6 --checksum blake2 -f
mkfs.ext4 /dev/mapper/part7 # Create the `/boot` filesystem
mkfs.btrfs /dev/mapper/homepart --checksum blake2 -f


mount /dev/mapper/part2 /mnt
mkdir -p /mnt/boot/efi
mount "$disk"1 /mnt/boot/efi
mkdir -p /mnt/parsec/update
mount /dev/mapper/part3 /mnt/parsec/update
cp "$boot_keyfile" /mnt/parsec/update/boot_keyfile --no-preserve=all
mkdir -p /mnt/parsec/flags
mount /dev/mapper/part4 /mnt/parsec/flags
mkdir -p /mnt/home
#mount /dev/mapper/part7 /mnt/boot
#mount "$disk"7 /mnt/boot
mount /dev/mapper/homepart /mnt/home

mkdir -p /mnt/parsec/installer
# Copy the installer to the newly-provisioned system's dm-verity partition
# This will allow users with $OS_NAME installed to provision new systems without re-downloading this script
cp "$0" /mnt/parsec/installer
cd /mnt/parsec/installer
#clear
cat << EOF
This installer can automatically download your system's rootfs for you. This is recommended. Alternatively, if you already have a copy of the rootfs, you can skip downloading it.

WARNING: If you use your own rootfs, this installer will skip integrity checking. To verify integrity yourself, you can compare with the following BLAKE2 hash:
> 812fb1983170664c6660f1ab5529586f98af1f8be8e3aaabe02f90f1db5bd724966a3583c5024bc74ab7a0fb990ab9eb74f64449e19fdebe9e0b7757485631af

If you select "y" to download it now, this installer will automatically verify integrity. This can provide extra security in the case where the download mirror is potentially compromised.

EOF
echo -n "Do you want to download the rootfs now? (Y/n) "
read dlnow
if [[ "$dlnow" == "n" || "$dlnow" == "N" ]]; then
	cat << EOF
You must now enter the path where this installer can find your rootfs image. If it's stored on an external drive, please insert the drive now (if you haven't already). If you haven't yet mounted the drive it's stored on, mount it now.
EOF
	echo -n "Path to rootfs: "
	read rootfspath
	cp "$rootfspath" ./void-x86_64-musl-ROOTFS-20210218.tar.xz # Copy the provided image onto the newly-provisioned system
	cat << EOF
Since you have decided to use your own rootfs image, integrity checking has been skipped. This means that this installer cannot protect you against a compromised rootfs image.
If you would like to abort this installation, hit CTRL+C to exit. If you are confident the provided rootfs image is safe, hit ENTER to continue installation.
EOF
	read
else
	cat << EOF
This installer is now downloading the rootfs it will use to install $OS_NAME. Once this is completed, it will install the base packages needed to boot to your new system.
EOF
	wget https://alpha.de.repo.voidlinux.org/live/current/void-x86_64-musl-ROOTFS-20210218.tar.xz -O void-x86_64-musl-ROOTFS-20210218.tar.xz #--no-check-certificate
b2check=$(b2sum -c << EOF
BLAKE2 (void-x86_64-musl-ROOTFS-20210218.tar.xz) = 812fb1983170664c6660f1ab5529586f98af1f8be8e3aaabe02f90f1db5bd724966a3583c5024bc74ab7a0fb990ab9eb74f64449e19fdebe9e0b7757485631af
EOF
)
	if [[ "$b2check" != "void-x86_64-musl-ROOTFS-20210218.tar.xz: OK" ]]; then
        	echo "Integrity check failed: BLAKE2"
		exit
	fi
fi
cd /mnt
cp /mnt/parsec/installer/void-x86_64-musl-ROOTFS-20210218.tar.xz /tmp/rootfs
tar -xvf /mnt/parsec/installer/void-x86_64-musl-ROOTFS-20210218.tar.xz
#clear
#for group in wheel xbuilder kvm floppy audio video cdrom optical; do
#	echo "Adding froup '$group'"
#	groupadd -R /mnt "$group"
#done
useradd -R /mnt admin
usermod -R /mnt -a -G wheel admin
useradd -R /mnt update
usermod -R /mnt -a -G wheel,xbuilder update
passwd -R /mnt -l root
cat << EOF
You must choose a username for your daily-use account. This user will be unprivileged and will not have 'sudo' access
To perform administrative tasks, you may login as 'admin' which has access to the 'sudo' command
If left blank, your username defaults to "user"

EOF
echo -n "Username: "
read user
if [[ "$user" == "" ]]; then
	user=user
fi
useradd -R /mnt "$user"

# Set $user to default user groups. This should be enough for usability, but not enough for the user to be privileged
# to the point of being a security risk.

cat << EOF
You may configure the groups your daily-use user will be in. Note that your daily-use user should not have root access
As such, please avoid giving your user access to the wheel group. Also, try to avoid giving your user access to groups
you don't need for daily use. A separate "admin" user will be created for tasks that require elevated privileges.

The default groups are:
	floppy,audio,video,cdrom,optical,kvm
If unsure, leave this blank to use the default.

EOF
echo -n "Groups (leave empty for default): "
read groups
if [[ "$groups" == "" ]]; then
	groups=floppy,audio,video,cdrom,optical,kvm
fi

usermod -R /mnt -a -G "$groups" "$user"
cat << EOF
You must set a password for your user. Try to choose a secure password. An attacker who gets this password can login to your account, given brief physical access to your system.
Alternatively, if they have remote access (via an exposed network service or another user having been already compromised), they can use this password to login as your user and read your files.

EOF
passwd -R /mnt "$user" || {
	echo "Failed to set user password."
	exit
}
cat << EOF
You must also set a password for your admin user. This password must be secure, since an attacker who gets this password can get root access and steal any sensitive information stored on this system, as well as modify the contents of users' home directories

EOF
passwd -R /mnt admin || {
	echo "Failed to set admin password."
	exit
}
cat << EOF
Your users are now configured! To login as your admin user, input "admin" as the username. From there you will be able to access commands like 'sudo'
EOF

# Backup default contents of `/home` and use it to reset contents at boot
# When compined with verified boot, this prevents bashrc persistence and can frustrate bashrc privilege escalation
# This is more effective when the file permissions are set to read-only, preventing attackers with non-root access
# from leveraging these files for privilege escalation. RO permissions should be restored each boot to ensure that
# attackers can't bypass this for privilege escalation simply by changing perms and waiting for the user to reboot

tar -c -f /mnt/parsec/installer/home-defaults.tar /home/

# Update xbps on the new system
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -Suy xbps -r /mnt || exit
#/usr/bin/xbps-install -uy -r /mnt # Update the new system

# Replace sudo with doas, then symlink for compatibility
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -y opendoas -r /mnt
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-remove -y sudo base-voidstrap -r /mnt
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -y linux -r /mnt # Install Linux in a separate transaction
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -y base-system -r /mnt
xbps-uchroot /mnt rm /usr/bin/sudo
chroot /mnt /sbin/ln -s /sbin/doas /usr/bin/sudo

tee -a /mnt/etc/doas.conf << EOF
permit :wheel
permit nopass root
EOF

# Install GRUB on the new system
SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -y grub-x86_64-efi -r /mnt || exit
#/usr/bin/xbps-install -y os-prober -r /mnt
#SSL_NO_VERIFY_PEER=0 /usr/bin/xbps-install -y linux -r /mnt || exit # Already installed as a dependency of `base-voidstrap`

# Install bootloader
# The target system must support UEFI, since this OS will not support legacy BIOS

# So, I've just done all this configuration. Let's hope it works!

echo "Setting up GRUB's encrypted /boot config..."
#echo 'GRUB_DISABLE_OS_PROBER=true' | tee -a /mnt/etc/default/grub
echo 'GRUB_ENABLE_CRYPTODISK="y"' | tee -a /mnt/etc/default/grub
echo 'GRUB_DISABLE_LINUX_RECOVERY="true"' | tee -a /mnt/etc/default/grub

echo -n 'GRUB_CMDLINE_LINUX="' | tee -a /mnt/etc/default/grub

#uuid="$(blkid "$disk"7 | cut -d\" -f2)"
#echo -n "cryptdevice=UUID=$uuid:/dev/mapper/part7 " | tee -a /mnt/etc/default/grub
uuid="$(blkid "$disk"2 | cut -d\" -f2)"
echo -n "cryptdevice=UUID=$uuid:/dev/mapper/part2 " | tee -a /mnt/etc/default/grub
uuid="$(blkid /dev/mapper/part2 | cut -d\" -f2)"
#echo -n 'root=UUID=$uuid ' | tee -a /mnt/etc/default/grub
echo -n 'root=/dev/mapper/part2"' | tee -a /mnt/etc/default/grub
echo | tee -a /etc/default/grub # Add trailing newline

uuid="$(blkid "$disk"2 | cut -d'"' -f2)"
echo "/dev/mapper/part2 / btrfs defaults,noatime,ro 0 1" | tee -a /mnt/etc/fstab
echo "part2 UUID=$uuid /crypto_keyfile.bin luks" | tee -a /mnt/etc/crypttab

#uuid="$(blkid "$disk"2 | cut -d'"' -f2)"
#echo "/dev/mapper/part7 / ext4 defaults,noatime,umask=0077,ro 0 2" | tee -a /mnt/etc/fstab
#echo "part7 UUID=$uuid /crypto_keyfile.bin luks" | tee -a /mnt/etc/crypttab

uuid="$(blkid "$disk""$home" | cut -d'"' -f2)"
echo "/dev/mapper/part$home / btrfs defaults,nosuid,nodev,umask=0077 0 3" | tee -a /mnt/etc/fstab
echo "part$home UUID=$uuid /crypto_keyfile.bin luks" | tee -a /mnt/etc/crypttab

uuid="$(blkid "$disk"1 | cut -d'"' -f2)"
echo "UUID=$uuid /boot/efi vfat umask=0077 0 0" | tee -a /mnt/etc/fstab

cat /mnt/etc/default/grub > /etc/default/grub
#cat /mnt/etc/default/grub > /mnt/etc/grub.d/
cat /mnt/etc/crypttab > /etc/crypttab
cat /mnt/etc/fstab > /etc/fstab

echo "Installing GRUB..."
xbps-uchroot /mnt mkdir /boot/grub
xbps-uchroot /mnt update-grub
#echo "Running grub-install..."
#grub-install --target=x86_64-efi --root-directory=/mnt --boot-directory=/mnt/boot --efi-directory=/mnt/boot/efi "$disk"
#echo "Installed GRUB. Updating config..."

uuid="$(blkid "$disk"2 | cut -d\" -f2 | tr -d -)"
#mapuuid="$(blkid /dev/mapper/part2 | cut -d\" -f2)"
addtocfg="$(
cat << EOF
insmod part_gpt
	insmod cryptodisk
	insmod luks
	insmod gcry_rijndael
	insmod gcry_rijndael
	insmod gcry_sha256
	cryptomount -u $uuid
	set root='cryptouuid/$uuid'
	
EOF
)"
old='if [ x$feature_platform_search_hint = xy ]; then'
new="$addtocfg"'if [ x$feature_platform_search_hint = xy ]; then'
script="/tmp/$(cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c24)"
tee "$script" << EOF
import sys
f = open(sys.argv[1], "r")
old_cfg = f.read()
f.close()
new_cfg = old_cfg.replace("""$old""", """$new""")
new_cfg = new_cfg.replace(
	"""search --no-floppy --fs-uuid --set=root  """,
	"""search --no-floppy --fs-uuid --set=root --hint='cryptouuid/$uuid'  """
)
new_cfg = new_cfg.replace("/boot/vmlinuz", "/parsec/boot/vmlinuz")
f = open(sys.argv[1], "w")
f.write(new_cfg)
f.close()
EOF
python "$script" /mnt/boot/grub/grub.cfg

cp "$keyfile" /crypto_keyfile.bin --no-preserve=all
dn="/tmp/$(cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c24)"
mkdir -p "$dn"
umount /mnt/boot/efi
mv /mnt/boot/* "$dn/"
mount /dev/mapper/part7 /mnt/boot
mv "$dn/"* /mnt/boot/
mount "$disk"1 /mnt/boot/efi
grub-install --target=x86_64-efi --root-directory=/mnt --boot-directory=/mnt/boot --efi-directory=/mnt/boot/efi "$disk"
cp -a /mnt/boot/* "$dn/"
umount /mnt/boot/efi
umount /mnt/boot
mkdir -p /mnt/parsec/boot
mv "$dn/"* /mnt/parsec/boot/
#touch /mnt/boot/isroot

# Finish install and activate verified boot
cat << EOF
Chrooting into your new system now. You can make changes here before your verified boot setup is locked.
When you're done making the necessary changes to your system, type "exit" to lock your verified boot.
After verified boot is locked, any changes to your system will need to happen in update mode.
EOF
xbps-uchroot /mnt /bin/bash
